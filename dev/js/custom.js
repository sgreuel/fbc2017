$(document).ready(function () {
  $('body').removeClass('no-js');
  $(".owl-carousel").owlCarousel({
    items: 1
  });
  $('body').on('click', '.horst-toggler', function (e) {
    e.preventDefault();
    $('body').toggleClass('nav-open');
  });

  $('body').on('click', '.card-header', function (e) {
    $('.card-icon').removeClass('fa-minus').addClass('fa-plus');
    $(this).toggleClass('open');
    $accordion_content = $(this).next('.acc');
    $('.acc').not($accordion_content).slideUp();
    $('.acc').not($accordion_content).prev('.card-header').removeClass('open');
    $accordion_content.stop(true, true).slideToggle(400);
    $('.card-icon', this).removeClass('fa-plus').addClass('fa-minus');
  });

  $('body').on('click', '.card-header.open', function (e) {
    $(this).find('.card-icon').removeClass('fa-minus').addClass('fa-plus');
  });

  $('body').on('click', '.link-base-home', function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: $('#buy').offset().top
    });
  });
});
