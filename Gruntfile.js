module.exports = function (grunt) {

    'use strict';

    grunt.initConfig({
        /**
         * Get package meta data
         */
        pkg: grunt.file.readJSON('package.json'),
        /**
         * Set project object
         */
        project: {
            app: './'
        },
        /**
         * Sass (compile & minify)
         */
        sass: {
            dist: {
                options: {
                    update: true,
                    style: 'expanded', /* compressed */
                    sourcemap: 'auto'
                },
                files: {
                    '<%= project.app %>assets/style.css': [
                        '<%= project.app %>dev/scss/style.scss'
                    ]
                }
            }
        },

        concat: {
          js: {
              options: {
                  separator: ';\n',
                  sourcemap: true
              },
              files: {
                  '<%= project.app %>assets/javascript.js': [
                      '<%= project.app %>bower_components/jquery/dist/jquery.min.js',
                      '<%= project.app %>bower_components/tether/dist/js/tether.js',
                      '<%= project.app %>bower_components/bootstrap/dist/js/bootstrap.js',
                      '<%= project.app %>bower_components/owl.carousel/dist/owl.carousel.js',
                      '<%= project.app %>dev/js/**/*.js'
                  ]
              }
          }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 8', 'ie 9']
            },
            your_target: {
                '<%= project.app %>assets/style_ap.css': [
                    '<%= project.app %>assets/style.css'
                ]
            }
        },

        uglify: {
          options: {
            sourcemap: true
          },
          my_target: {
              files: {
                  '<%= project.app %>assets/script.min.js': ['<%= project.app %>assets/javascript.js']
              }
          }
        },
        combine_mq: {
            new_filename: {
                options: {
                    beautify: true
                },
                src: '<%= project.app %>assets/style.css',
                dest: '<%= project.app %>assets/style.min.css'
            }
        },
        /**
         * Watch files for changes
         */
        watch: {
            sass: {
                files: '<%= project.app %>/dev/scss/**/*.scss',
                tasks: ['sass', 'autoprefixer', 'combine_mq', 'ftp-deploy'],
                options: {
                    spawn: false
                }
            },
            js: {
                files: ['<%= project.app %>/dev/js/*.js', '<%= project.app %>/assets/js/**/*.js'],
                tasks: ['concat:js', 'uglify', 'ftp-deploy'],
                options: {
                    spawn: false
                }
            }
        },

        'ftp-deploy': {
            build: {
                auth: {
                    host: 'favetto.de',
                    port: 21,
                    authKey: 'key1'
                },
                src: 'assets/../',
                dest: '/projekte/fbc2017/',
                exclusions: ['.idea', '.sass-cache', 'bower_components', 'dev', 'log', 'node_modules', '*.iml', '.git']
            }
        }
    });

    /**
     * Load Grunt plugins
     */
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    /**
     * Default task
     * Run `grunt` on the command line
     */
    grunt.registerTask('default', [
        'sass',
        'autoprefixer',
        'combine_mq',
        'concat:js',
        'uglify',
        'watch'
    ]);
};
